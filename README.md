#SimpleSerializer

**Author:** Jason Penick

**Email:** StrikeBackStudios@gmail.com

**Namespace:** SimpleSerializer

**Classes:** SimpleSerializerConfiguration, XMLSerializer

**Attributes:** SSClass, SSProperty, SSField

# Description
   
SimpleSerializer is a XML based Serializer/Deserializer.
Its purpose was to be able to quickly and easily serialize and deserialize
objects into a human readable format without the need to markup a class with
all sorts of attribute tags, which sometimes is not possible if you are
working with a compiled dll or pre-existing data types.

Its intended purpose was as a way to quickly save and load data classes
for either storage locally or transport over a network.

By using a set of simple and straight forward configuration options
SimpleSerialize is typically able to achieve the desired output of a class
without the need to use any attributes on classes, fields, or properties.

SimpleSerializer does allow the use of specifically designed
attributes, but they are intended to enhance the basic functionality
instead of be a requirement..

### Supported Data Types ###
* sbyte
* byte
* short
* ushort
* int
* uint
* long
* ulong
* char
* float
* double
* boolean
* decimal
* Strings
* Enums
* Arrays - Single and Multi-Dimensional
* List<>
* Dictionary<,>
* Custom Classes - Generics are not currently supported

# Example
```c-sharp
using SimpleSerializer;

public void test(){

    XMLSerializer ss = new XMLSerializer()

    List<int> intList = new List<int>();

    for (int i = 0; i < 1000; i++)

         intList.Add(i);

     string output = ss.Serialize(intList);
     List<int> rebuilt = ss.Deserialize(output);

}
```

### Things I would like to do: ###

1.         Extract specific objects out of the library instead of just the root object.
2.         Include support for generics outside of List<> and Dictionary<,>