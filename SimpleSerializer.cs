/*********************************************************************************************\
    Module Name: SimpleSerializer
    Author: Jason Penick
    Email: StrikeBackStudios@gmail.com

    Namespace: SimpleSerializer
    Classes: SimpleSerializerConfiguration, XMLSerializer
    Attributes: SSClass, SSProperty, SSField

    Description:
        SimpleSerializer is a XML based Serializer/Deserializer. Its purpose was to be able
    to quickly and easily serialize and deserialize objects into a human readable format
    without the need to markup a class with all sorts of attribute tags, which sometimes is
    not possible if you are working with a compiled dll.

       Its intended purpose was as a way to quickly save and load data classes for either
   storage locally or transport over a network.

        By using a set of simple and straight forward configuration options SimpleSerialize is
    able to achieve the desired output of a class without the need to use any attributes on
    classes, fields, or properties.

        SimpleSerializer does allow the use of specifically designed attributes, but they are
    intended to enhance the basic functionality instead of be a requirement.

    NOTE:
        SimpleSerializer currently does not support generics besides the List<> 
    and Dictionary<,> classes which specific checks have been put in place for.

    Things I would like to do:
        1) Extract specific objects out of the library instead of just the root object.
        2) Include support for generics outside of List<> and Dictionary<,>

\*********************************************************************************************/





using System;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Xml.Linq;
using System.Runtime.CompilerServices;


namespace SimpleSerializer
{
    /// <summary>
    /// XMLSerializer has the option to only serialize properties which contain the SSProperty Attribute.
    /// If that option is enabled this Attribute is required on any property you wish to serialize. Inclusion of
    /// the SSProperty attribute on a property does not guarantee that a property will be serialized regardless of other
    /// configuration rules.
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.All, AllowMultiple = false)]
    class SSProperty : System.Attribute{
        private string _OutputName;
        private string _GetValueFunction;
        private bool _IsGetValueOverriden;
        private string _SetValueFunction;
        private bool _IsSetValueOverriden;

        public SSProperty()
        {
            _OutputName = null;
            _IsGetValueOverriden = false;
            _IsSetValueOverriden = false;
        }

        public string OutputName
        {
            get { return _OutputName; }
            set { _OutputName = value; }
        }

        public virtual bool IsGetValueOverriden
        {
            get { return _IsGetValueOverriden; }
        }

        public virtual string GetValueFunction
        {
            get { return _GetValueFunction; }
            set
            {
                _IsGetValueOverriden = true;
                _GetValueFunction = value;
            }
        }

        public virtual bool IsSetValueOverriden
        {
            get { return _IsSetValueOverriden; }
        }

        public virtual string SetValueFunction
        {
            get { return _SetValueFunction; }
            set
            {
                _IsSetValueOverriden = true;
                _SetValueFunction = value;
            }
        }
    }

    /// <summary>
    /// XMLSerializer has the option to only serialize fields which contain the SSField Attribute.
    /// If that option is enabled this Attribute is required on any field you wish to serialize. Inclusion of
    /// the SSField attribute on a field guarantees that a field will be serialized regardless of other
    /// configuration rules.
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = false)]
    public class SSField : System.Attribute {
        private string _OutputName;
        private string _GetValueFunction;
        private bool _IsGetValueOverriden;
        private string _SetValueFunction;
        private bool _IsSetValueOverriden;

        public SSField()
        {
            _OutputName = null;
            _IsGetValueOverriden = false;
            _IsSetValueOverriden = false;
        }

        public string OutputName
        {
            get { return _OutputName; }
            set { _OutputName = value; }
        }

        public virtual bool IsGetValueOverriden
        {
            get { return _IsGetValueOverriden; }
        }

        public virtual string GetValueFunction
        {
            get { return _GetValueFunction; }
            set {
                _IsGetValueOverriden = true;
                _GetValueFunction = value;
            }
        }

        public virtual bool IsSetValueOverriden
        {
            get { return _IsSetValueOverriden; }
        }

        public virtual string SetValueFunction
        {
            get { return _SetValueFunction; }
            set
            {
                _IsSetValueOverriden = true;
                _SetValueFunction = value;
            }
        }

    }

    /// <summary>
    /// XMLSerializer has the option to only serialize classes which contain the SSClass Attribute.
    /// If that option is enabled this Attribute is required on any class you wish to serialize.
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Class, AllowMultiple = false)]
    class SSClass : System.Attribute { }



    /// <summary>
    /// Configuration class. Used to tell SimpleSerializer what it should serialize.
    /// </summary>
    public class SimpleSerializerConfiguration
    {
        /// <summary>If private fields should be saved</summary>
        public bool savePrivateFields = true;
        /// <summary>If we should serialize public fields or not</summary>
        public bool savePublicFields = true;
        /// <summary>If we should save Read Only fields or not.</summary>
        public bool saveReadOnlyFields = true;
        /// <summary>if we should save private properties or not.</summary>
        public bool savePrivateProperties = true;
        /// <summary>if we should save public properties or not.</summary>
        public bool savePublicProperties = true;
        /// <summary>if we should save properties that have no SET method.</summary>
        public bool saveReadOnlyProperties = false;
        /// <summary>if the element should contain a type attribute which contains the name of the data type of the object being serialized</summary>
        public bool includeDataType = true;
        /// <summary>/if comments should be included in the xml to make it easier to debug</summary>
        public bool includeComments = true;
        /// <summary>the maximum nested depth of objects that we will serialize.</summary>
        public int maxDepth = 10;
    }

    /// <summary>
    /// XMLSerializer is used to... wait for it.... Serialize data in to XML format.
    /// </summary>
    public class XMLSerializer
    {

        /// <summary> list of types we expect to commonly serialize and deserialize. Used to speed up locating unknown types. </summary>
        private static List<Type> registeredTypes = new List<Type>();

        /* Configuration Fields */
        private bool savePrivateFields = false;         // if we should serialize private fields or not
        private bool savePublicFields = true;           // if we should serialize public fields or not
        private bool saveReadOnlyFields = true;         // if we should save Read Only fields or not. 
        private bool savePrivateProperties = false;     // if we should save private properties or not.
        private bool savePublicProperties = true;       // if we should save public properties or not.
        private bool saveReadOnlyProperties = true;     // if we should save properties that have no SET method.
        private bool includeDataType = false;           // if the element should contain a type attribute which contains the name of the data type of the object being serialized
        private bool includeComments = false;           // if comments should be included in the xml to make it easier to debug
        private bool requireSSAttribute = false;        // when true only classes, fields, and properties with a SSClass, SSField, or SSProperty Attribute will be Serialized
        private int maxDepth = 10;                      // the maximum nested depth of objects that we will serialize.
        
        /// <summary> Pre-defined set of BindingFlags used to locate private fields and properties on objects</summary>
        public const BindingFlags PrivateBindingFlag = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy;
        /// <summary> Pre-defined set of BindingFlags used to locate private fields and properties on objects</summary>
        public const BindingFlags PublicBindingFlag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy;

        /* lists we use during serialization to build up our TOC and final output.*/
        private List<object> _SerializationObjectPool = new List<object>();
        private List<string> _SerializationObjectXml = new List<string>();

        /* lists we use during Deserialization to keep track of already desrialized objects, and a list of nodes of objects to be deserialized*/
        private List<XmlNode> _DeserializationNodes = new List<XmlNode>();
        private Dictionary<int, Object> _DeserializedObjects = new Dictionary<int, object>();

        /// <summary>
        /// Creates an instance of the XMLSerializer with default settings
        /// </summary>
        public XMLSerializer()
        {
            
        }

        /// <summary>
        /// Creates an instance of the XMLSerializer with the configuration options specified in the config parameter.
        /// </summary>
        /// <param name="config">SimpleSerializerConfiguration object containing configuration data for the XMLSerializer</param>
        public XMLSerializer(SimpleSerializerConfiguration config)
        {
            
            this.savePrivateFields = config.savePrivateFields;
            this.savePublicFields = config.savePublicFields;
            this.saveReadOnlyFields = config.saveReadOnlyFields;
            this.savePrivateProperties = config.savePrivateProperties;
            this.savePublicProperties = config.savePublicProperties;
            this.includeDataType = config.includeDataType;
            this.includeComments = config.includeComments;
            this.maxDepth = config.maxDepth;

        }

        /// <summary>
        /// Used to register a TYPE of an object you expect to serialize and deserialize often. This prevents 
        /// XMLSerializer from having to use reflection to look up previous unknown types and speeds up the process.
        /// </summary>
        /// <param name="type">The class TYPE that you expect to serialize or deserialize often.</param>
        public static void registerType(Type type)
        {

            if (registeredTypes.Contains(type))
                return; 

            registeredTypes.Add(type);
            return;

        }

        /// <summary>
        /// Used to unregister a TYPE of an object you no londer expect to serialize and deserialize often. Use this 
        /// when you no longer expect to have to serialize or de-serialize a type any longer. This will reduce the number
        /// of TYPEs XMLSerializer has to inspect to find the proper one.
        /// </summary>
        /// <param name="type">The class TYPE that you expect to serialize or deserialize often.</param>
        public void unregisterType(Type type)
        {
            if (registeredTypes.Contains(type))
                registeredTypes.Remove(type);
            return;
        }

        /// <summary>
        /// Serialize an object to an XML string.
        /// </summary>
        /// <param name="data">The object you want to serialize. Primatives, Enum, String, Non-generic Structures, Non-Generic Classes, List and Dictionary</param>
        /// <returns></returns>
        public string Serialize(object data)
        {
            StringBuilder sb = new StringBuilder();
            Serialize(data, 0);

            sb.Append("<toc>");
            for (int i = 0; i < _SerializationObjectXml.Count; i++)
            {
                sb.Append(_SerializationObjectXml[i]);
            }
            sb.Append("</toc>");

            string output = sb.ToString();
            _SerializationObjectPool.Clear();
            _SerializationObjectXml.Clear();
            return output;
        }

        private String Serialize(object data, int depth )
        {

            depth++;

            /*if data is null we just return an empty string*/
            if (data == null)
                return string.Empty;

            /*if we are at max depth we just return an empty string*/
            if (depth > this.maxDepth)
                return string.Empty;


            Type dataType = data.GetType();     //find out what type of data we are dealing with.

            /* This should never happen but ya never know, so better safe then sorry.*/
            if (dataType == null)
            {
                throw new System.ArgumentException("dataType==null : This should never happen....");
            }

            /*
             * You can not serialize Abstract Classes, Interface Classes, or static classes.
             * Static classes are marked Abstract and Sealed so checking for IsAbstract handles both Abstract classes and STATIC classes
             */
            if (dataType.IsAbstract || dataType.IsInterface)
            {
                throw new System.InvalidOperationException("You can not serialize Abstract or Interface classes.");
            }

            StringBuilder sb = new StringBuilder();

            /*
             * This handles the below Types
             * Boolean, Byte, SByte, Int16, UInt16, Int32, UInt32, Int64, UInt64, 
             * IntPtr, UIntPtr, Char, Double, and Single, Enum, String
             */
            if (depth == 1)
            {
                if (dataType.IsPrimitive || dataType.IsEnum || (data is string))
                {
                    _SerializationObjectXml.Add(CreateXMLElement("libObj", dataType.Name, XMLEncode(data.ToString()), true));
                    return string.Empty;
                }
                    

                /*if (dataType.IsArray)
                {
                    int arrayID = _SerializationObjectPool.Count;
                    _SerializationObjectPool.Add(data);
                    string tOut = arrayID.ToString();
                    _SerializationObjectXml.Add(tOut);//we do this as a placeholder
                    tOut = CreateXMLElement("libObj", typeof(Array).Name, dataType.GetElementType().Name, SerializeArray(data, depth), true);
                    _SerializationObjectXml[arrayID] = tOut;
                    return string.Empty;
                }*/

            }

            if ((dataType.IsPrimitive || dataType.IsEnum || (data is string)) && !dataType.IsArray)
                return XMLEncode(data.ToString()); //this seems to cover everythin nicely... below code was left in place just in case...



            /*
             * Ok we are parsing some sort of custom class object
             */


            if (data.GetType().IsValueType && !dataType.IsArray)             //object is a ValueType class (a structure)
            {
                return SerializeClass(data, depth);     //So we directly serialize it in place instead of storing it in the object library
            }

            int libraryID = getObjectLibraryID(data);   //try and get the objects id. If the object has been serialized we will get a number >=0 otherwise we will get -1

            if (libraryID != -1)                        //check and see if we found the object
            {
                return String.Empty;                    //we did so we dont serialize anything, just return an empty string.
            }

            /*
             * Ok, so the object isnt a primative, enum, string, or structure, and the object has not been serialized yet.
             */

            int nextID = _SerializationObjectPool.Count;                    // find what our objects ID is going to be.
            _SerializationObjectPool.Add(data);                             // Add the object to the list of serialized objects. We do this before actually serializing it in case something else holds a reference to this object.
            _SerializationObjectXml.Add(nextID.ToString());                 // Add a temporary value in to the XML data as place holder.
            _SerializationObjectXml[nextID] = SerializeClass(data, depth);  // store the serialized data in list of XML for objects.

            return String.Empty;
            
        }

        private string SerializeArray(object data, int depth)
        {
            Type dataType = data.GetType();
            if (dataType.IsArray == false)
                return String.Empty;

            Type arrayType = dataType.GetElementType();
            Array array = (Array)data;
            StringBuilder output = new StringBuilder();

            int dimensions = array.Rank;
            int endPos = dimensions - 1;
            int[] sizes = new int[dimensions];
            
            int[] offset = new int[dimensions];

            for (int i = 0; i < dimensions; i++)
            {
                offset[i] = 0;
                sizes[i] = array.GetLength(i);
            }

            int size = sizes[0];

            for (int i = 1; i < sizes.Length; i++)
                size *= sizes[i];

            for (int i = 0; i < size; i++)
            {


                bool useType = false;
                object obj = array.GetValue(offset);
                Type objType = obj.GetType();

                if (objType != arrayType)
                    useType = true;

                if (IsComplexType(objType) && !objType.IsValueType)
                {
                    int id = getObjectLibraryID(obj);
                    if (id == -1)
                    {
                        Serialize(obj, depth);
                        id = getObjectLibraryID(obj);
                    }

                    output.Append(CreateXMLElement("i", objType.Name, id, useType));
                }
                else
                {
                    output.Append(CreateXMLElement("i", objType.Name, Serialize(obj, depth), useType));
                }

                offset[endPos]++;
                for (int n = endPos; n >= 0; n--)
                {
                    if (offset[n] >= sizes[n])
                    {
                        offset[n] = 0;
                        if (n > 0 )
                            offset[n - 1]++;
                    }
                }
            }

            
            
            return output.ToString();
        }

        private string SerializeList(object data, int depth)
        {
            Type dataType = data.GetType();
            if (IsList(dataType) == false)
                return String.Empty;


            
            Type listType = dataType.GetGenericArguments()[0];
            IList list = (IList)data;
            StringBuilder output = new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                bool useType = false;
                object item = list[i];
                Type itemType = item.GetType();
                if (listType != itemType)
                    useType = true;

                if (IsComplexType(itemType))
                {
                    if (_SerializationObjectPool.Contains(item))
                    {
                        int id = getObjectLibraryID(item);
                        
                        output.Append(CreateXMLElement("i", itemType.Name, id, useType));
                    }
                    else
                    {
                        string sData = Serialize(list[i], depth);
                        int id = getObjectLibraryID(item);
                        output.Append(CreateXMLElement("i", itemType.Name, id, useType));

                        
                    }
                }
                else
                {
                    
                    output.Append(CreateXMLElement("i", itemType.Name, Serialize(item, depth), useType));
                }
                
                
            }
            return output.ToString();
        }

        private string SerializeDictionary(object data, int depth)
        {
            Type dataType = data.GetType();
            if (IsDictionary(dataType) == false)
                return String.Empty;

            Type keyType = dataType.GetGenericArguments()[0];
            Type valueType = dataType.GetGenericArguments()[1];
            IDictionary list = (IDictionary)data;
            StringBuilder output = new StringBuilder();
            IList keys = list.Keys  as Array;
            bool useType = false;

            foreach(object key in list.Keys)                //loop through all the keys in the dictionary
            {
                object val = list[key];                     //get the value ok the key from the dictionary
                Type realType = key.GetType();              //store the realType of the key
                output.Append("<i>");                       //append the Item XML open tag

                if (keyType != realType)                    //check if the keys real type matches the dictionaries key type
                    useType = true;                         //it doesnt so we set useType to true so that store the keys real type in the XML with it.
                
                if (IsComplexType(keyType) && !keyType.IsValueType)     //check if the Key is some sort of complex object, but also not a structure (ValueType)
                {                                                       //ok Key is an object, but not a struct
                    int id = getObjectLibraryID(key);                   //check and see if we have already serialized this object.

                    if (id == -1)                                       //getObjectLibraryID return -1 if no object was found.
                    {
                        Serialize(key, depth);                          //object hasnt been serialized yet so we serialize it now.
                        id = getObjectLibraryID(key);                   //now we find the objects ID
                    }
                    
                    output.Append(CreateXMLElement("k", realType.Name, id, useType));   //append the XML element for the key to the output.
                }else {                                                                 //ko we are either delaing with a primative, enum, string, or structure
                    output.Append(CreateXMLElement("k", realType.Name, Serialize(key, depth), useType));
                }


                useType = false;                                        //reset useType to false so we can use it again below when checking the objects store in the dictionaries values.
                realType = val.GetType();                               //re-use the realType variable to get the real TYPE of the value object.
                if (valueType != realType)                         //check and see if valueType matches the objects actual type
                    useType = true;                                     //it doesnt so we set useType to true so we know to store the value of the type in the XML element

                if (IsComplexType(realType) && !realType.IsValueType)     //check and see if we are working with a complex object, but also not a structure (ValueType)
                {
                    int id = getObjectLibraryID(val);                               //see if the object has been serialized already by trying to get its id in the list.
                    if (id == -1)                                                   //we have not serialized the object yet since getObjectLibraryID returns -1 if the object was not found.
                    {
                        Serialize(val, depth);                                      // object wasnt serialized yet, so well do it now.
                        id = getObjectLibraryID(val);                               //now we find the id of the object.
                    }
                    output.Append(CreateXMLElement("v", realType.Name, id, useType));      //write out the XML element for the value of this key
                    
                }else{                                                                          // ok the value is some sort of primative, enum, string or structure (ValueType)
                    output.Append(CreateXMLElement("v", realType.Name, Serialize(val, depth), useType));
                }
                output.Append("</i>");                      //append the XML closing tag for the item

            }
            return output.ToString();                       //return the string containing the representation of this dictionary
        }

        private string SerializeClass( object data, int depth)
        {

            Type dataType = data.GetType();

            StringBuilder output = new StringBuilder();

            if (dataType.IsArray)
            {
                Array array = data as Array;
                int dimensions = array.Rank;
                
                int[] sizes = new int[dimensions];

                for (int i = 0; i < dimensions; i++)
                    sizes[i] = array.GetLength(i);
                
                return CreateArrayXMLElement("libObj", typeof(Array).Name, sizes, dataType.GetElementType().Name, SerializeArray(data, depth), true);
            }

            if (IsDictionary(dataType))
            {
                return String.Format("<libObj type=\"{0}\" keyType=\"{1}\" valueType=\"{2}\" >{3}</libObj>", dataType.Name, dataType.GetGenericArguments()[0].Name, dataType.GetGenericArguments()[1].Name, SerializeDictionary(data, depth));
            }

            if (IsList(dataType))
            {
                return String.Format("<libObj type=\"{0}\" generic=\"{1}\" >{2}</libObj>", dataType.Name, dataType.GetGenericArguments()[0].Name, SerializeList(data, depth));
            }

            if (requireSSAttribute)
            {
                if (dataType.GetCustomAttributes(typeof(SSClass), true).Length == 0) //Check to see if we have the SSField attribute
                    return string.Empty;
            }
            output.Append("<libObj type=\"" + data.GetType().Name + "\">");

            // if (depth == 1)
            // {
            //output.Append("<" + data.GetType().Name + ">");
            // }

            if (this.savePrivateFields)
            {
                if (this.includeComments)
                    output.Append("<!--Private Field-->");
                output.Append(SerializeFields(data, depth, XMLSerializer.PrivateBindingFlag));
            }

            if (this.savePublicFields)
            {
                if (this.includeComments)
                    output.Append("<!--Public Field-->");
                output.Append(SerializeFields(data, depth, XMLSerializer.PublicBindingFlag));
            }

            if (this.savePrivateProperties)
            {
                if (this.includeComments)
                    output.Append("<!--Private Properties-->");
                output.Append(SerializeProperties(data, depth, XMLSerializer.PrivateBindingFlag));
            }

            if (this.savePublicProperties)
            {
                if (this.includeComments)
                    output.Append("<!--Public Properties-->");
                output.Append(SerializeProperties(data, depth, XMLSerializer.PublicBindingFlag));
            }

            //if (depth == 1)
                output.Append("</libObj>");

            return output.ToString();

        }

        /// <summary>
        /// Serializes the Fields of an object.
        /// </summary>
        /// <param name="data">The object whose fields will be serialzed.</param>
        /// <param name="depth">The current object depth of the serialization process</param>
        /// <param name="bindingFlags">used to determine which fields we want to serializes.</param>
        /// <returns>A string the contains the XML representation for the object</returns>
        private string SerializeFields(object data, int depth, BindingFlags bindingFlags = BindingFlags.Default)
        {
            /* null does have fields, so we cant serialize it, so lets throw an exception*/
            if (data == null)
            {
                throw new System.ArgumentException("data paramater passed with null value");
            }

            Type dataType = data.GetType();                 //Get the Type class of the object we are dealing with
            StringBuilder output = new StringBuilder();     //StringBuilder class to build up the XML
            FieldInfo[] fields = dataType.GetFields(bindingFlags); //Get a list of the fields using the bindingFlags provided.

            foreach (FieldInfo field in fields)     //loop through all the fields
            {

                /*ignore compiler generated fields for now*/
                if (field.IsDefined(typeof(CompilerGeneratedAttribute), false))
                    continue;

                bool ssField = false;               //Used to determine if this filed has the SSField Attribute which forces serialization

                if (field.GetCustomAttributes(typeof(SSField), true).Length > 0) //Check to see if we have the SSField attribute
                    ssField = true;                                              //we do so we set ssField to true to signal that.

                
                if (field.IsInitOnly && !saveReadOnlyFields & !ssField)     //Determine if we should be serializing this field
                    continue;                                               // we arent so we continue on.



                
                object obj = field.GetValue(data);
                Type fieldType = field.GetType();
                string fieldName = field.Name;

                if (ssField)
                {
                    SSField attribute = (SSField) field.GetCustomAttributes(typeof(SSField), true)[0];

                    if (attribute.OutputName != null)
                        fieldName = attribute.OutputName;

                    if (attribute.IsGetValueOverriden == true)
                    {
                        string methodName = attribute.GetValueFunction;
                        MethodInfo outputFun = dataType.GetMethod(methodName, PublicBindingFlag | PrivateBindingFlag);
                        if (outputFun != null)
                        {
                            obj = outputFun.Invoke(data, null);
                            fieldType = obj.GetType();
                        }
                    }
                }


                if (obj == null)
                    continue;

                if ((obj != null) && IsComplexType(obj.GetType()) && !obj.GetType().IsValueType)
                {
                    int id = getObjectLibraryID(obj);
                    if (id == -1)
                    {
                        Serialize(field.GetValue(data), depth);
                        id = getObjectLibraryID(obj);
                    }
                    output.Append(CreateXMLElement(fieldName, fieldType.Name, id, includeDataType));
                }
                else {
                    output.Append(CreateXMLElement(fieldName, fieldType.Name, Serialize(obj, depth), includeDataType));
                }
                
            }

            return output.ToString();   //return the built up XML string representing the field.
        }

        /// <summary>
        /// Serializes the properties of an object.
        /// </summary>
        /// <param name="data">The object whose propeties will be serialized.</param>
        /// <param name="depth">The current object depth of the serialization process</param>
        /// <param name="bindingFlags">used to determine which property we want to serializes.</param>
        /// <returns>A string the contains the XML representation for the object</returns>
        private string SerializeProperties(object data, int depth, BindingFlags bindingFlags = BindingFlags.Default)
        {

            /* null does have properties, so we cant serialize it, so lets throw an exception*/
            if (data == null)
            {
                throw new System.ArgumentException("data paramater passed with null value");
            }

            StringBuilder output = new StringBuilder();         //StringBuilder is used to build up our XML output
            Type dataType = data.GetType();                     //get the Type class for the object whose properties we want to serialize.

            PropertyInfo[] props = data.GetType().GetProperties(bindingFlags);      //get an array of Properties on this object using the bindingFlags specified
            foreach (PropertyInfo prop in props)                //loop through all the properties in the array
            {
                bool readOnly = true;                           //Used to determine if the property is 'readOnly'
                bool isSSProperty = false;                      //User to determine if the property has the SSProperty flag which forces serialization

                readOnly = (prop.GetSetMethod() == null) ? true : false;        //Determine if the property has a 'set' method. If not its considered 'read only'

                if (prop.GetCustomAttributes(typeof(SSProperty), true).Length >= 1)     //check to see if the property has the SSPRoperty Attribute
                    isSSProperty = true;                                                //It does has the SSProperty so lets set the isSSProperty variables

                if ((requireSSAttribute == true) && (!isSSProperty))
                    continue;

                if (!readOnly || isSSProperty || (readOnly && saveReadOnlyProperties)) //check if we should serialize the property
                {

                    object obj = prop.GetValue(data, null);
                    Type propertyType = prop.GetType();
                    string propertyName = prop.Name;

                    if (isSSProperty)
                    {
                        SSProperty attribute = (SSProperty)prop.GetCustomAttributes(typeof(SSProperty), true)[0];

                        if (attribute.OutputName != null)
                            propertyName = attribute.OutputName;

                        if (attribute.IsSetValueOverriden == true)
                        {
                            string methodName = attribute.SetValueFunction;
                            MethodInfo outputFun = dataType.GetMethod(methodName, PublicBindingFlag | PrivateBindingFlag);
                            if (outputFun != null)
                            {
                                obj = outputFun.Invoke(data, null);
                                propertyType = obj.GetType();
                            }
                        }
                    }


                    if (obj == null)
                        continue;
                    if (IsComplexType(propertyType) && !propertyType.IsValueType)
                    {
                        int id = getObjectLibraryID(obj);
                        if (id == -1)
                        {
                            Serialize(obj, depth);
                            id = getObjectLibraryID(obj);
                        }
                        output.Append(CreateXMLElement(propertyName, propertyType.Name, id, includeDataType));
                        
                    }
                    else {
                        // we are so we output XML to include type="prop.PropertyType.Name"
                        output.Append(CreateXMLElement(propertyName, propertyType.Name, Serialize(obj, depth), includeDataType));
                    }
                }
            }

            return output.ToString(); //return the built up XML string representing the field.
        }

        private string CreateXMLComment(string comment, bool force = false)
        {
            if (this.includeComments || force)
                return String.Format("<!--{0}-->", comment);

            return String.Empty;

        }

        private string CreateArrayXMLElement(string name, string typename, int[] dimensions, string generic, string value, bool useType)
        {
            if (useType)
            {
                string dimensionString = string.Empty;
                for (int i = 0; i < dimensions.Length - 1; i++)
                    dimensionString += String.Format("{0},", dimensions[i].ToString());

                dimensionString += dimensions[dimensions.Length - 1].ToString();
                return string.Format("<{0} type=\"{1}\" size=\"{2}\" generic=\"{3}\">{4}</{0}>", name, typename, dimensionString, generic, value);
            }

            return string.Format("<{0}>{1}</{0}>", name, value);
        }

        /// <summary>
        /// Used to build the the XML Element that represents a class, field, or property
        /// </summary>
        /// <param name="name">The name of the element to be created.</param>
        /// <param name="value">The value of the element to be created.</param>
        /// <returns>A string containing a valid XMLElement for the data provided.</returns>
        private string CreateXMLElement(string name, string value)
        {
            return string.Format("<{0}>{1}</{0}>", name, value);
        }

        private string CreateXMLElement(string name, string typename, string value, bool useType)
        {
            if (useType)
                return string.Format("<{0} type=\"{1}\" >{2}</{0}>", name, typename, value);

            return string.Format("<{0}>{1}</{0}>", name, value);
        }

        private string CreateXMLElement(string name, string typename, string generic, string value, bool useType)
        {
            if (useType)
                return string.Format("<{0} type=\"{1}\" generic=\"{2}\">{3}</{0}>", name, typename, generic, value);

            return string.Format("<{0}>{1}</{0}>", name, value);
        }

        private string CreateXMLElement(string name, int id)
        {
            return string.Format("<{0} libid=\"{1}\" />", name, id);
        }

        private string CreateXMLElement(string name, string typename, int id)
        {
            return string.Format("<{0} type=\"{1}\" libid=\"{2}\" />", name, typename, id);
        }

        private string CreateXMLElement(string name, string typename, int id, bool useType)
        {
            if (useType)
                return string.Format("<{0} type=\"{1}\" libid=\"{2}\" />", name, typename, id);

            return string.Format("<{0} libid=\"{1}\" />", name,id);
        }

        /// <summary>
        /// Used to build the the XML Element that represents a class, field, or property. Includes a typeName paramater that is included in the element denote the datas Type.
        /// </summary>
        /// <param name="name">Name of the elment to be created.</param>
        /// <param name="typeName">The name of the Type this element represents.</param>
        /// <param name="value">The value of the element to be created.</param>
        /// <returns></returns>
        private string CreateXMLElement(string name, string typeName, string value)
        {
            return string.Format("<{0} type=\"{1}\">{2}</{0}>", name, typeName, value);
        }

        private string CreateXMLElement(string name, string typeName, Type[] generics, string value)
        {
            string gNames = String.Empty;

            foreach(Type t in generics)
            {
                gNames = String.Format("{0}#{1}", gNames, t.Name);

            }
            return string.Format("<{0} type=\"{1}{2}\">{3}</{0}>", name, typeName, gNames, value);

        }

        #region Deserialization Functions

        /// <summary>
        /// Deserializes an XML based string generated from SimpleSerializer back into an object.
        /// </summary>
        /// <param name="xmlData">the string containing the xml data representing the object</param>
        /// <returns></returns>
        public object Deserialize(string xmlData)
         {
            XmlReaderSettings readerSettings = new XmlReaderSettings();
            readerSettings.IgnoreComments = true;

            XmlReader reader = XmlReader.Create(new StringReader(xmlData), readerSettings);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(reader);

            XmlNode library = xmlDoc.ChildNodes[0];
            int childCount = library.ChildNodes.Count;


            for (int i = 0; i < childCount; i++)
            {
                XmlNode libObj = library.ChildNodes[i];
                _DeserializationNodes.Add(libObj);
            }

            XmlNode primeNode = _DeserializationNodes[0];
            string typeName = primeNode.Attributes["type"].Value;
            Type type = findDataType(typeName);
            object obj = Deserialize(type, primeNode);
            _DeserializedObjects.Clear();
            _DeserializationNodes.Clear();
            return obj;

         }

        /// <summary>
        /// Deserializes an XML based string generated from SimpleSerializer back into an object, returns the object as the TYPE specified by T
        /// </summary>
        /// <param name="xmlData">the string containing the xml data representing the object</param>
        /// <returns></returns>
        public T Deserialize<T>(string xmlData) where T : class
        {
            // Console.WriteLine("DATA.length: " + xmlData.Length.ToString());
            // Console.WriteLine("Data in bytes: " + System.Text.ASCIIEncoding.Unicode.GetByteCount(xmlData).ToString());
            XmlReaderSettings readerSettings = new XmlReaderSettings();
            readerSettings.IgnoreComments = true;

            XmlReader reader = XmlReader.Create(new StringReader(xmlData), readerSettings);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(reader);


            XmlNode library = xmlDoc.ChildNodes[0];
            int childCount = library.ChildNodes.Count;

            string typeName;
            Type objectType;

            for (int i = 0; i < childCount; i++)
            {
                XmlNode libObj = library.ChildNodes[i];
                _DeserializationNodes.Add(libObj);
            }

            XmlNode primeNode = _DeserializationNodes[0];
            T val = Deserialize(typeof(T), primeNode) as T;
            _DeserializedObjects.Clear();
            _DeserializationNodes.Clear();
            return val as T;
        }

        private object Deserialize(Type type, XmlNode node)
        {

            if (node.Attributes != null && (node.Attributes["libid"] != null))
            {
                int id = int.Parse(node.Attributes["libid"].Value);
                if (_DeserializedObjects.ContainsKey(id))
                    return _DeserializedObjects[id];

                XmlNode objNode = _DeserializationNodes[id];
                return Deserialize(type, objNode);

            }

            if (type == typeof(object))
            {
                Type nodeType = getNodeType(node);

                if ((nodeType != null) && (type != nodeType))
                {
                    type = nodeType;
                }
            }
            if (type.IsPrimitive || (type == typeof(string)))
            {
                return Convert.ChangeType(node.InnerText, type);
            }

            if (type.IsEnum)
            {
                return Enum.Parse(type, node.InnerText);
            }

            if (IsList(type))
            {
                return DeserializeList(type, node);
            }

            if (IsDictionary(type))
            {
                return DeserializeDictionary(type, node);
            }

            if (type.IsArray)
            {
                return DeserializeArray(type, node);
            }

            

            /*complex class*/

            return DeserializeClass(type, node);
        }

        private object DeserializePrimative(Type type, XmlNode node)
        {
            return Convert.ChangeType(node.InnerText, type);
        }

        private object DeserializeArray(Type type, XmlNode node)
        {
            int itemCount = node.ChildNodes.Count;

            Type arrayType = type.GetElementType();

            Type nodeType = getNodeGeneric(node);
            if (nodeType != null)
                arrayType = nodeType;

            

            int[] sizes = getArrayNodeSizes(node);
            int dimensions = sizes.Length;
            int endPos = dimensions - 1;
            int[] offset = new int[dimensions];

            Array array = Array.CreateInstance(arrayType, sizes);

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {
                XmlNode itemNode = node.ChildNodes[i];

                object val = Deserialize(arrayType, itemNode);
                array.SetValue(val, offset);

                offset[endPos]++;
                for (int n = endPos; n >= 0; n--)
                {
                    if (offset[n] >= sizes[n])
                    {
                        offset[n] = 0;
                        if (n > 0)
                            offset[n - 1]++;
                    }
                }
            }
            return array;
        }

        private object DeserializeList(Type type, XmlNode node)
        {
            
            int itemCount = node.ChildNodes.Count;

            Type elementType;

            if (node.Attributes["generic"] != null)
                elementType = findDataType(node.Attributes["generic"].Value);
            else
             elementType = type.GetGenericArguments()[0];
            

            var listInstance = (IList)typeof(List<>).MakeGenericType(elementType).GetConstructor(Type.EmptyTypes).Invoke(null);

            for (int n = 0; n < itemCount; n++)
            {
                    XmlNode itemNode = node.ChildNodes[n];
                    if (itemNode.Attributes["type"] != null)
                        elementType = findDataType(itemNode.Attributes["type"].Value);
                    
                    object val = Deserialize(elementType, itemNode);
                    listInstance.Add(val);
            }
            return listInstance;
        }

        private object DeserializeDictionary(Type type, XmlNode node)
        {
            Type keyType = type.GetGenericArguments()[0];
            Type valueType = type.GetGenericArguments()[1];
            var dictionaryInstance = (IDictionary)typeof(Dictionary<,>).MakeGenericType(keyType, valueType).GetConstructor(Type.EmptyTypes).Invoke(null);

            if (node.ChildNodes.Count == 0)
            {

                return null;// dictionaryInstance;
            }

            int itemCount = node.ChildNodes.Count;

            for (int n = 0; n < itemCount; n++)
            {
                    
                XmlNode itemNode = node.ChildNodes[n];
                XmlNode keyNode = itemNode.ChildNodes[0];
                XmlNode valueNode = itemNode.ChildNodes[1];
                
                if (valueNode.Attributes["type"] != null)
                    valueType = findDataType(valueNode.Attributes["type"].Value);
                
                object dictionaryKey = Deserialize(keyType, keyNode);
                object dictionaryValue = Deserialize(valueType, valueNode);
                dictionaryInstance.Add(dictionaryKey, dictionaryValue);
            }

            return dictionaryInstance;
        }

        private object DeserializeClass(Type type, XmlNode node)
        {
            

            if (type.Name == "List`1")
                return DeserializeList(type, node);

            if (type.Name == "Dictionary`2")
                return DeserializeDictionary(type, node);

            if (type == typeof(Array))
            {
                return DeserializeArray(type, node);
            }

            Type nodeType = getNodeType(node);

            if ((nodeType != null) && (type != nodeType))
            {
                Console.WriteLine("*******DeserializeClass - TYPE CHANGED!!!!" + nodeType.Name);
                type = nodeType;
            }

            MethodInfo method = type.GetMethod("deserialize", new Type[] { typeof(XmlNode) });

            if (method != null)
            {
                return method.Invoke(null, new object[] { node });
            }

            object newObject = Activator.CreateInstance(type);

            DeserializeClassFields(newObject, node);
            return newObject;
        }

        private void DeserializeClassFields(object newObject, XmlNode node)
        {
            Type objectType = newObject.GetType();
            FieldInfo[] fields = objectType.GetFields();
            PropertyInfo[] properties = objectType.GetProperties();

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {
                XmlNode currentNode = node.ChildNodes[i];
                string eName = node.ChildNodes[i].Name;
                string eValue = node.ChildNodes[i].InnerText;

                FieldInfo field = objectType.GetField(eName, XMLSerializer.PrivateBindingFlag);
                if (field == null)
                    field = objectType.GetField(eName, XMLSerializer.PublicBindingFlag);
                if (field != null)
                {
                    object obj = Deserialize(field.FieldType, currentNode);
                    if (field.GetCustomAttributes(typeof(SSField), true).Length > 0)
                    {
                        

                        SSField attribute = (SSField)field.GetCustomAttributes(typeof(SSField), true)[0];
                        if (attribute != null)
                        {

                            if (attribute.IsSetValueOverriden == true)
                            {
                                string methodName = attribute.SetValueFunction;
                                MethodInfo outputFun = objectType.GetMethod(methodName, PublicBindingFlag | PrivateBindingFlag);
                                if (outputFun != null)
                                {
                                    outputFun.Invoke(newObject, new object[] { obj });
                                    continue;
                                }
                            }
                        }
                    }
                    field.SetValue(newObject, obj);
                    continue;
                }

                PropertyInfo property = objectType.GetProperty(eName, XMLSerializer.PrivateBindingFlag);
                if (property == null)
                    property = objectType.GetProperty(eName, XMLSerializer.PublicBindingFlag);

                if (property != null)
                {
                    object obj = Deserialize(property.PropertyType, currentNode);

                    if (property.GetCustomAttributes(typeof(SSProperty), true).Length > 0)
                    {
                        SSProperty attribute = (SSProperty)field.GetCustomAttributes(typeof(SSProperty), true)[0];
                        if (attribute != null)
                        {

                            if (attribute.IsSetValueOverriden == true)
                            {
                                string methodName = attribute.SetValueFunction;
                                MethodInfo outputFun = objectType.GetMethod(methodName, PublicBindingFlag | PrivateBindingFlag);
                                if (outputFun != null)
                                {
                                    outputFun.Invoke(newObject, new object[] { obj });
                                    continue;
                                }
                            }
                        }
                    }


                    if (property.GetSetMethod() != null)
                    {
                        property.SetValue(newObject, obj, null);
                        continue;
                    }
                    continue;
                }
            }

            return;

        }

        #endregion

        private bool isObjectSerialized(object obj)
        {
            if (_SerializationObjectPool.Contains(obj))
                return true;

            return false;
        }

        private int getObjectLibraryID(object obj)
        {
            int count = _SerializationObjectPool.Count;
            for (int i = 0; i < count; i++)
            {
                if (_SerializationObjectPool[i] == obj)
                    return i;
            }

            return -1;
        }

        private Type getNodeType(XmlNode node)
        {
            if (node.Attributes["type"] != null)
            {
                string typeName = node.Attributes["type"].Value;
                return findDataType(typeName);
            }
            return null;
        }

        private Type getNodeGeneric(XmlNode node)
        {

            if (node.Attributes == null)
                return null;

            if (node.Attributes["generic"] == null)
                return null;

            string typeName = node.Attributes["generic"].Value;

            Type type = findDataType(typeName);

            return type;
        }

        private int[] getArrayNodeSizes(XmlNode node)
        {
            if (node.Attributes != null)
            {
                if (node.Attributes["size"] != null)
                {
                    string[] dimStr = node.Attributes["size"].Value.Split(',');
                    int[] results = new int[dimStr.Length];
                    for (int i = 0; i < dimStr.Length; i++)
                    {
                        results[i] = int.Parse(dimStr[i]);
                        
                    }
                    return results;
                }
            }

            return new int[] { };
        }

        private Type findDataType(string name)
        {
            for (int i = 0; i < registeredTypes.Count; i++)
            {
                if (registeredTypes[i].Name == name)
                    return registeredTypes[i];
            }
            
            Assembly[] asms = AppDomain.CurrentDomain.GetAssemblies();

            for (int i = 0; i < asms.Length;i++)
            {

                Type[] types = asms[i].GetTypes();

                if (types == null)
                    continue;

                for (int n = 0; n < types.Length; n++)
                    if (types[n].Name == name)
                    {
                        registeredTypes.Add(types[n]);
                        return types[n];
                    }


            }
            return null;
        }
        
        #region Static Functions Definitions 

        /// <summary>
        /// Determines if the given TYPE is a List
        /// </summary>
        /// <param name="type">The TYPE you wish to check</param>
        /// <returns>True if type is a List, otherwise False</returns>
        public static bool IsList(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition().IsAssignableFrom(typeof(List<>));
        }

        /// <summary>
        /// Determines if the given TYPE is a Dictionary
        /// </summary>
        /// <param name="type">The TYPE you wish to check</param>
        /// <returns>True if the TYPE is a Dictionary, otherwise False</returns>
        public static bool IsDictionary(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition().IsAssignableFrom(typeof(Dictionary<,>));
        }

        /// <summary>
        /// Determines if the given type is Primative, Enum, or String
        /// </summary>
        /// <param name="type">The TYPE you wish to check</param>
        /// <returns>True is the TYPE is a Primative, Enum, or String. Otherwise False</returns>
        public static bool isBasicType(Type type)
        {

            if (type == null)
            {
                throw new System.ArgumentException("dataType==null : This should never happen....");
            }

            if (type.IsPrimitive || type.IsEnum || (type == typeof(string)))
                return true;

            if (type.IsClass || type.IsInterface)
                return false;



            return false;


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">The TYPE you wish to check</param>
        /// <returns>True if the object is not a primative, enum or string. Otherwise False.</returns>
        public static bool IsComplexType(Type type)
        {
            if ( (type.IsEnum == false) && (type.IsPrimitive == false) && type != (typeof(string)))
                return true;
            return false;
        }

        /// <summary>
        /// replaces invalid XML characters with their HTML encoded equivelent.
        /// </summary>
        /// <param name="data">String you want to encode</param>
        /// <returns>An encoded version of the input string</returns>
        public static string XMLEncode(string data)
        {
            return data.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;");
        }

        #endregion
    }



    

}